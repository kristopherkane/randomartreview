from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash

import os

app = Flask(__name__)
app.config.from_object(__name__)
app.config['PROPAGATE_EXCEPTIONS'] = True
base_data_dir = os.environ['OPENSHIFT_DATA_DIR']

@app.route('/')
def show_entries():
    with open (base_data_dir + "/randomart.out", "r") as art:
        data=art.readlines()
        art_on_a_line = ""
        for line in data:
            art_on_a_line += line
    return render_template('index.html', art=art_on_a_line)

if __name__ == '__main__':
    app.run()
