#!/bin/bash

YEAR=`date +%Y`
MONTH=`date +%m`
DAY=`date +%d`
HOUR=`date +%H`
MINUTE=`date +%M`
SECOND=`date +%S`

mkdir -p $OPENSHIFT_DATA_DIR/data/$YEAR/$MONTH/$DAY
mkdir -p $OPENSHIFT_DATA_DIR/tmp

if [ -f tmp/key ];
 then
    rm tmp/key
fi

ssh-keygen -f tmp/key -N '' | grep '+\||' > $OPENSHIFT_DATA_DIR/data/$YEAR/$MONTH/$DAY/$HOUR.out

ln -s $OPENSHIFT_DATA_DIR/data/$YEAR/$MONTH/$DAY/$HOUR.out $OPENSHIFT_DATA_DIR/randomart.out.new && \
mv -Tf $OPENSHIFT_DATA_DIR/randomart.out.new $OPENSHIFT_DATA_DIR/randomart.out


