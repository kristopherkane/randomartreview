from setuptools import setup

setup(name='randomart.gallery',
      version='0.2',
      description='Daily ssh-keygen randomart',
      author='Kristopher Kane',
      author_email='kristopher.kane@gmail.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=['Flask==0.10.1'],
     )
